"""Jinja2 extensions."""
import json
import string
import uuid
from secrets import choice

from jinja2.ext import Extension
from slugify import slugify as pyslugify


class UnderlinedExtension(Extension):
    """Jinja2 extension to create a random string."""

    def __init__(self, environment):
        """Jinja2 Extension Constructor."""
        super().__init__(environment)

        def underlined(title, *args, symbol="-", above=False):
            title = title.format(*args)
            bar = symbol * len(title)
            if above:
                return f"{bar}\n{title}\n{bar}"
            else:
                return f"{title}\n{bar}"

        environment.globals.update(underlined=underlined)
