# Cookiecutter template for Model Data Explorer Apps

This repository provides the possibility to generate the basic structure of an
app for the model data explorer.

You can generate a new app with the following commands:

```bash
git clone https://gitlab.hzdr.de/model-data-explorer/mde-app-template
PYTHONPATH="./mde-app-template/:$PYTHONPATH" cookiecutter mde-app-template/
```

This will prompt for some input, including a `project_name` under which you
will then find the skeleton of a new app.
