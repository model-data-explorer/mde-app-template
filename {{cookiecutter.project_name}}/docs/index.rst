.. {{ cookiecutter.project_name }} documentation master file, created by
   sphinx-quickstart on Mon Feb 21 15:15:53 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

{{ underlined("Welcome to {}'s documentation!", cookiecutter.project_name, symbol="=") }}

.. rubric:: {{ cookiecutter.project_short_description }}

.. warning::

    This package is work in progress, especially it's documentation.
    Stay tuned for updates and discuss with us at
    https://gitlab.hzdr.de/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_name }}


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   configuration
   api
   contributing



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
