.. _configuration:

Configuration options
=====================

Configuration settings
----------------------

The following settings have an effect on the app.

.. automodulesumm:: {{ cookiecutter.package_folder }}.app_settings
    :autosummary-no-titles:
