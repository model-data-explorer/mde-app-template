.. _api:

API Reference
=============

.. toctree::
    :maxdepth: 1

    api/{{ cookiecutter.package_folder }}.app_settings
    api/{{ cookiecutter.package_folder }}.urls
    api/{{ cookiecutter.package_folder }}.models
    api/{{ cookiecutter.package_folder }}.views


.. toctree::
    :hidden:

    api/modules
