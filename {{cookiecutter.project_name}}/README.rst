{{ underlined("{}", cookiecutter.project_title, symbol="=", above=True) }}

{{ cookiecutter.project_short_description }}

Installation
------------

Install this package in a dedicated python environment via::

    python -m venv venv
    source venv/bin/activate
    pip install {{ cookiecutter.project_name }}

To use this in a development setup, clone the `source code`_ from gitlab, start
the development server and make your changes::

    git clone https://gitlab.hzdr.de/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_name }}
    cd {{ cookiecutter.project_name }}
    python -m venv venv
    source venv/bin/activate
    make dev-install

More detailed installation instructions my be found in the docs_.




.. _source code: https://gitlab.hzdr.de/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_name }}
.. _docs: {{ cookiecutter.documentation_url }}installation.html


Copyright
---------
{{ cookiecutter.copyright.replace("(C)", "©") }}

Licensed under the EUPL-1.2-or-later

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the EUPL-1.2 license for more details.

You should have received a copy of the EUPL-1.2 license along with this
program. If not, see https://www.eupl.eu/.
