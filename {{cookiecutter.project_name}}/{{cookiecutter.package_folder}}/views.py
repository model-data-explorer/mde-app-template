"""Views
-----

Views of the {{ cookiecutter.project_name }} app to be imported via the url
config (see :mod:`{{ cookiecutter.package_folder }}.urls`).
"""

# Disclaimer
# ----------
#
# {{ cookiecutter.copyright }}
#
# This file is part of {{ cookiecutter.project_name }} and is released under the
# EUPL-1.2 license.
# See LICENSE in the root of the repository for full licensing details.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 or later
# as published by the European Commission.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# EUPL-1.2 license for more details.
#
# You should have received a copy of the EUPL-1.2 license along with this
# program. If not, see https://www.eupl.eu/.


from __future__ import annotations

from django.views import generic  # noqa: F401

from {{ cookiecutter.package_folder }} import app_settings  # noqa: F401
from {{ cookiecutter.package_folder }} import models  # noqa: F401
