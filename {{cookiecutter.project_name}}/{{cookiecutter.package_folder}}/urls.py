"""URL config
----------

URL patterns of the {{ cookiecutter.project_name }} to be included via::

    from django.urls import include, path

    urlpatters = [
        path(
            "{{ cookiecutter.project_name }}",
            include("{{ cookiecutter.package_folder }}.urls"),
        ),
    ]
"""

# Disclaimer
# ----------
#
# {{ cookiecutter.copyright }}
#
# This file is part of {{ cookiecutter.project_name }} and is released under the
# EUPL-1.2 license.
# See LICENSE in the root of the repository for full licensing details.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 or later
# as published by the European Commission.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# EUPL-1.2 license for more details.
#
# You should have received a copy of the EUPL-1.2 license along with this
# program. If not, see https://www.eupl.eu/.
from __future__ import annotations

from typing import Any

from django.urls import path  # noqa: F401

from {{ cookiecutter.package_folder }} import views  # noqa: F401

#: App name for the {{ cookiecutter.project_name }} to be used in calls to
#: :func:`django.urls.reverse`
app_name = "{{ cookiecutter.package_folder }}"

#: urlpattern for the Helmholtz AAI
urlpatterns: list[Any] = []
